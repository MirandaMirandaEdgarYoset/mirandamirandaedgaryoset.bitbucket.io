
var Ajedrez = (function() {

  var _llenaTabla=function(archivo){//funcion que recibe el contenido del archivo y lo procesa para llenar la tabla
    var tablero=document.getElementById('tablero');
    tablero.setAttribute("align","center");
    var tablita=document.createElement("table");///creo el elemento tabla
    tablita.setAttribute("id","tablita");
    var thead=document.createElement("thead");
    var tbody=document.createElement("tbody");
    var lineas = archivo.split("\n");
    var trCabecera=document.createElement("tr");
    var tdCab= document.createElement("td");
    tdCab.textContent="#";
    trCabecera.appendChild(tdCab);
    var cabeceras=lineas[0].split("|");
    for(var i=0;i<cabeceras.length;i++){
      var tdx=document.createElement("td");
      tdx.textContent=cabeceras[i];
      trCabecera.appendChild(tdx);
    }
    thead.appendChild(trCabecera);
    tablita.appendChild(thead);
        for(var i = 1; i < lineas.length-1; i++){
          var tr=document.createElement("tr");
            var fila=lineas[i].split("|");
            var tdNum=document.createElement("td");
            tdNum.textContent=i;
            tr.appendChild(tdNum);
              for(var j=0;j<fila.length;j++){
              var td=document.createElement("td");
                //var
                td.textContent=fila[j];
                td.setAttribute("id",cabeceras[j]+i);
                tr.appendChild(td);
              }
              tbody.appendChild(tr);
          }
      tablita.appendChild(tbody);
      tablero.appendChild(tablita);
  };

  var _cargaArchivo=function(){ //funcion que hace la solicitud del archivo tablero.csv al servidor
    const url ="csv/tablero.csv";
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
      if (this.readyState === 4) {
        var mensaje = document.getElementById("mensaje");
        mensaje.textContent = "";
        if (this.status === 200){
        //  console.log(xhr.responseText);
          _llenaTabla(xhr.responseText);
        } else {
            mensaje.textContent = "Error " + this.status + " " + this.statusText + " - " + this.responseURL;
        }
      }
    };
    xhr.open('GET', url, true);
    xhr.setRequestHeader('Access-Control-Allow-Headers', '*');
    xhr.send();
  };

  var _mostrar_tablero = function() {
    _cargaArchivo();
  };

  var _actualizar_tablero= function() {
  var tablita=document.getElementById('tablita');
  var padre=document.getElementById('tablero');
  padre.removeChild(tablita);
  _mostrar_tablero();
};
var agregaButton=function() {
  var padre=document.getElementById('opcion');
  var boton=document.createElement("input");
  boton.setAttribute("type","button");
  boton.setAttribute("id","actualizar");
  boton.setAttribute("value","Actualizar");
  boton.onclick=_actualizar_tablero;
  padre.appendChild(boton);
};

var _mover_pieza = function(obj_parametros) {
  var men=document.getElementById('mensaje');
  men.textContent="";
  var de=document.getElementById(obj_parametros.de);
  var a=document.getElementById(obj_parametros.a);
  if (de.textContent!="\u2205"&&a.textContent=="\u2205"){
    var aux=a.textContent;
    console.log(aux);
    a.textContent=de.textContent;
    console.log(a);
    de.textContent=aux;
  }
  else{
    men.textContent="NO SE PUEDE MOVER LA PIEZA";
  }
};

agregaButton();
return {"mostrarTablero": _mostrar_tablero,"actualizarTablero": _actualizar_tablero,"moverPieza": _mover_pieza}
})();
